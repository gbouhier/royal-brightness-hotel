// load plugins
var gulp = require('gulp'),
	sass = require('gulp-sass');
	autoprefixer = require('gulp-autoprefixer'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	notify = require('gulp-notify'),
	livereload = require('gulp-livereload'),
	plumber = require('gulp-plumber'),
	path = require('path'),
	minifycss = require('gulp-minify-css'),
	uglify = require('gulp-uglify');

// settings
var paths = {
	wp_theme : 'www/wp-content/themes/royalbrightness',
	styles: {
		src: 'src/scss/**/*.scss',
		dest: 'www/wp-content/themes/royalbrightness/css'
	},
	scripts: {
		src: [
			'src/js/vendors/*.js',
			'src/js/*.js',
			'src/js/partials/**/*.js'
		],
		dest: 'www/wp-content/themes/royalbrightness/js'
	}
};

// the title and icon that will be used for the Grunt notifications
var notifyInfo = {
	title: 'Gulp',
	icon: path.join(__dirname, 'gulp.png')
};

// error notification settings for plumber
var plumberErrorHandler = { errorHandler: notify.onError({
	title: notifyInfo.title,
	icon: notifyInfo.icon,
	message: "Error: <%= error.message %>"
})
};


/*===========================
=            DEV            =
===========================*/

// styles
gulp.task('styles', function () {
	return gulp.src([paths.styles.src])
		.pipe(plumber(plumberErrorHandler))
		.pipe(sass())
		.pipe(gulp.dest(paths.styles.dest));
});

// scripts
gulp.task('scripts', function() {
	return gulp.src(paths.scripts.src)
		.pipe(plumber(plumberErrorHandler))
		.pipe(concat('main.js'))
		.pipe(gulp.dest(paths.scripts.dest));
});

// dev
gulp.task('watch', function () {
	livereload.listen();

	//watch .scss files
	gulp.watch(paths.styles.src, ['styles']);

	// watch js files
	gulp.watch(paths.scripts.src, ['scripts']);

	// livereload
	gulp.watch([paths.wp_theme + '/**/*.php', paths.wp_theme + '/**/*.twig', paths.styles.dest + '/*.css', paths.scripts.dest + '/*.js'], function(event) {
		gulp.src(event.path)
			.pipe(plumber())
			.pipe(livereload());
	});

});


/*============================
=            PROD            =
============================*/

 // styles
 gulp.task('prod-styles', function () {
	 return gulp.src([paths.styles.src])
 		.pipe(plumber(plumberErrorHandler))
 		.pipe(sass())
 		.pipe(autoprefixer({ browsers: ['> 5%'] }))
 		.pipe(minifycss())
 		.pipe(gulp.dest(paths.styles.dest));
 });

 // scripts
 gulp.task('prod-scripts', function() {
 	return gulp.src(paths.scripts.src)
 		.pipe(plumber(plumberErrorHandler))
 		.pipe(concat('main.js'))
 		.pipe(uglify())
 		.pipe(gulp.dest(paths.scripts.dest));
 });

 // prod
 gulp.task('prod', ['prod-styles', 'prod-scripts']);

