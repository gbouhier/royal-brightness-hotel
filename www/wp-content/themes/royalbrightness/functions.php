<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action(
		'admin_notices', function () {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	}
	);

	return;
}

show_admin_bar(false);


// Custom classes
require_once 'class/Autoloader.php';
Autoloader::register();

new Rb_Cleanup();
new Rb_Site();
new Rb_Articles();
new Rb_Mail();

