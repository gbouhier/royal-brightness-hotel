<?php
/**
 * Template Name: Ménages professionnels
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();

$context['header']['title'] = get_field('header_title');
$context['header']['background'] = get_field('header_background');

$context['content']['title'] = get_field('content_title');
$context['content']['text'] = get_field('content_text');

$context['prestations_title'] = get_field('prestations_title');
$context['prestations'] = get_field('prestations');

Timber::render( 'pages/page-professionnals.twig', $context );