<?php
/**
 * Template Name: Accueil
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();

// Header
$context['header']['title'] = get_field('header_title');
$context['header']['baseline'] = get_field('header_text');
$context['header']['background'] = get_field('header_bg');

// Services
$context['services'] = get_field('services');
$context['services_title'] = get_field('services_title');

// Pourquoi nous
$context['arguments'] = get_field('arguments');
$context['arguments_title'] = get_field('arguments_title');

Timber::render( 'pages/page-home.twig', $context );