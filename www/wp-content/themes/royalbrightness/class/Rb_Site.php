<?php

class Rb_Site extends TimberSite {

	function __construct() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );

		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );

		add_action( 'init', array( $this, 'rb_header_scripts' ) ); // Add Custom Scripts to wp_head
		add_action( 'wp_enqueue_scripts', array( $this, 'rb_styles' ) ); // Add Theme Stylesheet
		add_action( 'init', array( $this, 'register_rb_menu' ) ); // Add rb Menu
		$this->register_rb_sidebar();

		add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );

		$this->add_options_page();

		add_shortcode( 'email', array( 'Rb_Helpers', 'wpcodex_hide_email_shortcode' ) );

		# Activate shortcode handling in widgets (e.g. for spam protection)
		add_filter( 'widget_text', 'shortcode_unautop' );
		add_filter( 'widget_text', 'do_shortcode' );

		// load_theme_textdomain('rb', get_template_directory() . '/languages');

		parent::__construct();
	}

	function add_to_context( $context ) {

		$context['site']       = $this;
		$context['assetsPath'] = get_template_directory_uri();

		$context['isFrontPage'] = is_front_page();

		$context['mainMenu']   = new TimberMenu( 'header-menu' );
		$context['mobileMenu'] = new TimberMenu( 'mobile-menu' );
		$context['footerMenu'] = new TimberMenu( 'footer-menu' );

		$context['footerWidget1'] = Timber::get_widgets( 'footer-widget-area-1' );
		$context['footerWidget2'] = Timber::get_widgets( 'footer-widget-area-2' );
		$context['footerWidget3'] = Timber::get_widgets( 'footer-widget-area-3' );

		// URL des Pages
		$context['urlDiscipline']   = Rb_Helpers::get_page_url_by_template( 'templates/page-discipline.php' );
		$context['urlAssociation']  = Rb_Helpers::get_page_url_by_template( 'templates/page-association.php' );
		$context['urlMembership']   = Rb_Helpers::get_page_url_by_template( 'templates/page-membership.php' );
		$context['urlOffice']       = Rb_Helpers::get_page_url_by_template( 'templates/page-office.php' );
		$context['urlContributors'] = Rb_Helpers::get_page_url_by_template( 'templates/page-contributors.php' );
		$context['urlActivities']   = Rb_Helpers::get_page_url_by_template( 'templates/page-activities.php' );
		$context['urlPartners']     = Rb_Helpers::get_page_url_by_template( 'templates/page-partners.php' );

		// Options from page options (return adress, phone, presentation etc)
		$context['options'] = get_fields('options');

		// Mail
		$context['contactEmail'] = antispambot('contact@rb-official.com');

		// Page slug from page template
		$post = new TimberPost();
		preg_match( '/templates\/page-([a-zA-z\-]+)\.php/', $post->_wp_page_template, $matches );
		$context['body_slug'] = isset($matches[1]) ? $matches[1]: '';

		return $context;
	}

	// Load rb scripts
	function rb_header_scripts() {
		if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {

			wp_register_script( 'rb_scripts', get_template_directory_uri() . '/js/main.js', array(), '1.1' ); // Custom scripts
			wp_enqueue_script( 'rb_scripts' ); // Enqueue it!
		}
	}

	// Load styles
	function rb_styles() {
		wp_register_style( 'rb', get_template_directory_uri() . '/css/style.css', array(), '1.1', 'all' );
		wp_enqueue_style( 'rb' );
	}

	// Register rb menus
	function register_rb_menu() {
		register_nav_menus(
			array(
				'header-menu' => __( 'Header Menu', 'rb' ), // Main Navigation
				'mobile-menu' => __( 'Mobile Menu', 'rb' ) // Mobile main menu
			)
		);
	}

	function register_rb_sidebar() {

		if ( function_exists( 'register_sidebar' ) ) {

			register_sidebar(
				array(
					'name'          => __( 'Footer Widget Area 1', 'rb' ),
					'id'            => 'foot-widget-area-1',
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h3>',
					'after_title'   => '</h3>'
				)
			);

			register_sidebar(
				array(
					'name'          => __( 'Footer Widget Area 2', 'rb' ),
					'id'            => 'foot-widget-area-2',
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h3>',
					'after_title'   => '</h3>'
				)
			);

			register_sidebar(
				array(
					'name'          => __( 'Footer Widget Area 3', 'rb' ),
					'id'            => 'foot-widget-area-3',
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h3>',
					'after_title'   => '</h3>'
				)
			);

		}
	}

	function get_breadcrumb() {

		global $post;

		// Homepage
		$breadcrumb = array(
			array(
				'name' => 'Accueil',
				'url'  => home_url()
			)
		);

		// Parents
		foreach ( get_post_ancestors( $post ) as $parent ) {
			$breadcrumb[] = array(
				'name' => get_the_title( $parent ),
				'url'  => get_permalink( $parent )
			);
		}

		// CTP : Article
		$news_page = get_option( 'page_for_posts' );
		if ( get_post_type() == "post" && is_single() ) {

			$breadcrumb[] = array(
				'name' => get_the_title( $news_page ),
				'url'  => get_permalink( $news_page ),
			);

		}

		// Is news list page
		if ( is_home() ) {

			$breadcrumb[] = array(
				'name' => get_the_title( $news_page )
			);
		}
		else {
			// Curent page
			$breadcrumb[] = array(
				'name' => get_the_title()
			);
		}
		return $breadcrumb;

	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( new Twig_SimpleFilter( 'autonbsp', array( $this, 'auto_nbsp' ) ) );

		return $twig;
	}

	// Twig filter auto non breaking space
	function auto_nbsp( $string ) {
		$string = str_replace( ' ?', '&nbsp;?', $string );
		$string = str_replace( ' !', '&nbsp;!', $string );
		$string = str_replace( ' :', '&nbsp;:', $string );

		return $string;
	}

	function add_options_page() {

		if ( function_exists( 'acf_add_options_page' ) ) {

			acf_add_options_page();

		}

	}

}
