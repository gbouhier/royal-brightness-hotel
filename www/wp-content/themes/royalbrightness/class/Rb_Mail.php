<?php

class Rb_Mail {

	function __construct() {

		add_filter( "wp_mail_content_type", array( $this, "mail_content_type" ) );
		add_filter( 'wp_mail', array( $this, "custom_mail_template" ) );
		//add_action( 'acf/save_post', array( $this, 'save_user_profile' ), 1 );

	}

	function mail_content_type() {

		return "text/html";

	}

	/**
	 * Use a custom mail template
	 */
	function custom_mail_template( $args ) {

		ob_start();
		get_template_part( 'mail' );
		$tpl = ob_get_clean();

		$tpl = str_replace( "{{mail_content}}", nl2br( $args["message"] ), $tpl );
		$tpl = str_replace( '[site_url]', get_site_url(), $tpl );

		$args["message"] = $tpl;

		return $args;

	}

	/**
	 * Send confirmation mail
	 *
	 * @param $email user email
	 * @param $type  paypal|cheque
	 */
	static function send_confirmation_mail( $email, $type ) {

		$title   = get_field( "email_confirmation_{$type}_title", "option" );
		$message = get_field( "email_confirmation_{$type}_message", "option" );

		self::_send_mail( $email, $title, $message );

	}


	/**
	 * Send mail
	 * Generic mailer function
	 */
	private static function _send_mail( $email, $subject, $message ) {

		// Process tokens
		$tokens = array();

		$user = get_user_by( 'email', $email );
		if ( $user ) {

			$user_id = $user->data->ID;

			$hash = new Hashids\Hashids( WEIDF_SALT, WEIDF_USER_HASH_LENGTH );

			$tokens['user_hash']  = $hash->encode( $user->data->ID );

			$tokens['first_name'] = get_field( "first_name", "user_" . $user_id );
			$tokens['last_name']  = get_field( "last_name", "user_" . $user_id );

		}

		$tokens['site_url'] = get_site_url();

		foreach ( $tokens as $key => $token ) {
			$message = str_replace( "[{$key}]", $token, $message );
		}

		// From
		$from_name  = get_field( 'email_from_name', 'option' );
		$from_email = get_field( 'email_from_email', 'option' );

		// Headers
		$headers = "From: " . stripslashes_deep( html_entity_decode( $from_name, ENT_COMPAT, 'UTF-8' ) ) . " <$from_email>\r\n";
		$headers .= "Reply-To: " . $from_email . "\r\n";

		return wp_mail( $email, html_entity_decode( $subject ), $message, $headers );

	}


}