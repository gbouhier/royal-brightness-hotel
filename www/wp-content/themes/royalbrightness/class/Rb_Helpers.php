<?php

class Rb_Helpers {

	public static function get_page_by_template( $template ) {

		$pages = query_posts(
			array(
				'post_type'  => 'page',
				'meta_key'   => '_wp_page_template',
				'meta_value' => $template
			)
		);

		return isset( $pages[0] ) ? $pages[0] : null;

	}

	public static function get_page_url_by_template( $template ) {

		$page = self::get_page_by_template( $template );
		return $page ? get_page_link( $page ) : '';

	}

	public static function get_page_id_by_template( $template ) {

		$page = self::get_page_by_template( $template );
		return $page ? $page->ID : 0;

	}

	/**
	 * Returns a safe filename, for a given platform (OS), by
	 * replacing all dangerous characters with an underscore.
	 *
	 * @param string $dangerousFilename The source filename
	 * to be "sanitized"
	 * @param string $platform The target OS
	 *
	 * @return string A safe version of the input
	 * filename
	 */
	public static function sanitize_file_name($dangerousFilename, $platform = 'Unix')
	{
		if (in_array(strtolower($platform), array('unix', 'linux'))) {
			// our list of "dangerous characters", add/remove
			// characters if necessary
			$dangerousCharacters = array(" ", '"', "'", "&", "/", "\\", "?", "#");
		} else {
			// no OS matched? return the original filename then...
			return $dangerousFilename;
		}

		// every forbidden character is replaced by an underscore
		return str_replace($dangerousCharacters, '_', $dangerousFilename);
	}



	/**
	 * Hide email from Spam Bots using a shortcode.
	 *
	 * @param array  $atts    Shortcode attributes. Not used.
	 * @param string $content The shortcode content. Should be an email address.
	 *
	 * @return string The obfuscated email address.
	 */
	 public static function wpcodex_hide_email_shortcode( $atts , $content = null ) {
		if ( ! is_email( $content ) ) {
			return;
		}

		return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
	}
}