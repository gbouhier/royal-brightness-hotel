<?php

class Rb_Articles {

	// Construct
	function __construct() {

		add_action( 'post_type', array( $this, 'toto' ) );

	}

	// Get all posts unformated
	public static function get_articles_posts( $filters = null ) {

		global $paged;
		if ( ! isset( $paged ) || ! $paged ) {
			$paged = 1;
		}

		$args = array(
			'post_type' => 'post',
			'paged'     => $paged,
		);

		if ( isset( $_GET['filter'] )  || isset($filters) ) {

			$params = ( isset($_GET['filter']) ) ? $_GET['filter'] : $filters;
			$params = str_replace( '/', '', $params );
			$params = explode( ",", $params );

			$args['meta_query'] = array(
				array(
					'key'     => 'type_article',
					'value'   => $params,
					'compare' => 'IN'
				)
			);
		}

		query_posts( $args );

		$posts = Timber::get_posts();

		return $posts;

	}

	// Get posts by ('category', 'temoignages', 'random');
	public static function get_articles_by( $type, $value,  $sortBy =  null, $maxNumber = null ) {

		$args = array(
			'orderby' => ($sortBy != null) ? $sortBy : 'date',
			'posts_per_page' => ($maxNumber != null) ? $maxNumber : ''
		);

		if ($type == 'category') {
			$args['category_name'] = $value;
		}

		elseif ($type == 'type') {
			$args['meta_query']= array(
					array(
						'key'     => 'type_article',
						'value'   => $value,
						'compare' => 'IN'
					)
				);
		}

		$posts = query_posts( $args );

		foreach ($posts as $post) {

			$cat_infos = get_the_category($post->ID);
			$cat_infos = $cat_infos[0];

			$articles[] = array(
				'id'      => $post->ID,
				'title'   => $post->post_title,
				'excerpt' => $post->post_excerpt,
				'thumb'   => new TimberImage( get_post_thumbnail_id( $post->ID ) ),
				'date'    => $post->post_date,
				'link'    => get_permalink( $post ),
				'catName' => $cat_infos->cat_name,
				'type'    => get_field( 'type_article', $post->ID ),
				'authorQuoteName' => ($value == 'testimonial') ? get_field('author_quote_name', $post->ID) : '',
				'authorQuoteRole' => ($value == 'testimonial') ? get_field('author_quote_role', $post->ID) : '',
			);

		}

		return $articles;

	}

	// Return all posts formated
	public static function get_articles() {

		$posts = self::get_articles_posts();

		$articles = array();
		foreach ( $posts as $post ) {

			$cat_infos = get_the_category($post->ID);
			$cat_infos = $cat_infos[0];

			// If the format if the default one, WP call it 'false', so we rename it to 'article' !
			$format = get_post_format( $post->ID );
			//$format = ( $format ) ? $format : 'article';

			switch ($format) {
				case 'gallery' :
					$format = 'photo';
					break;
				case false :
					$format = 'article';
					break;
			}

			$articles[] = array(
				'id'      => $post->ID,
				'title'   => $post->post_title,
				'excerpt' => $post->post_excerpt,
				'thumb'   => new TimberImage( get_post_thumbnail_id( $post->ID ) ),
				'date'    => $post->post_date,
				'link'    => get_permalink( $post ),
				'catName' => $cat_infos->cat_name,
				'type'    => get_field( 'type_article', $post->ID ),
			);

		}

		return $articles;
	}


}