<?php

/**
 * Class Autoloader
 * Autoloader
 */
class Autoloader {

	public static function register() {
		spl_autoload_register( array( __CLASS__, 'autoload' ) );
	}

	public static function autoload( $class ) {

		$class_filename =  $class . '.php';

		if ( file_exists( dirname( __FILE__ ) . '/' . $class_filename ) ) {
			require_once( $class_filename );
		}

	}

}