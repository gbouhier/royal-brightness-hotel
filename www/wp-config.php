<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'royalbrightness');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/_)tR|&_n!f/MwSwjQK|.3v-.mj!@D?>Y~p4%kaSP>[,WQ8m+#Deogcuq!P&I${T');
define('SECURE_AUTH_KEY',  'n{:p/g;h_Q4x_|nMBw^Y)K/s$Yulx{1@3#$?7PwFht&fI3Q#8urj>Z).sTbb5&vv');
define('LOGGED_IN_KEY',    '[55J*z#b5eS1XE iWjr)f6V_5YwhzrgJ;e&L_9&Z [LLABnH64!jilK:43RaY%3,');
define('NONCE_KEY',        'lx&4|B-Cj*BSY~ur<@}J%C3ck2mI(.boJ/lJcgj?6]~y}Oicq,6$dc&)2%>=mz@H');
define('AUTH_SALT',        'jxLp@*D)~=`!_3:#jB>&Xdv$Ub;0,,&64!_x[2V}o%h(JeQwwC[EfzEbD <%I A5');
define('SECURE_AUTH_SALT', '0SWF#ZnF{,IE5VV#LBP[yAAPh=DpM_2xowc%=dl#~%Fxn|TME*)EmQ0LknI7Av~?');
define('LOGGED_IN_SALT',   'jahQ4s[_^1A`<s&zWEM&NS<7dU::S &mSz0*u3T(0YKy}3B70Y=$WOIPt:~cSD8?');
define('NONCE_SALT',       'L(Wzdi}m4CDh^KdSSBQ$&Z_IHD2I!M]O^6cL36h*H54>>(1y~2Nf C`OnBn$WiLR');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'rb_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d'information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');