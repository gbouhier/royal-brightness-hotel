var App = {

	isIE: /*@cc_on!@*/false || !!document.documentMode,

	init: function () {

		this.bindEvents();
		this.minHeightMain();

		if ($("body").hasClass('home')) {
			App.Home.init();
		}



	},

	minHeightMain: function() {
		var windowHeight = $(window).height();
		var header = $('.header').height();
		var footer = $('.footer').height();
		var minHeight = windowHeight - (header + footer);
		console.log(minHeight);
		$('.main').css('min-height', minHeight);
	},

	bindEvents: function () {

		// Click on open mobile menu
		$(".open-mobile-menu").on('click', function () {
			$(".mobile-menu .menu-items").toggleClass("menu-open");
			$("body").toggleClass("overflow-hidden");
			$(".mobile-menu-overlay").toggle();

		});
	},


	isMobile: function () {
		return $("#ismobile").is(":visible");
	},

	isTablet: function () {
		return $("#istablet").is(":visible");
	},

	isDesktop: function () {
		return ( !this.isTablet() && !this.isMobile() ) ? true : false;
	}

};

$(document).ready(function ($) {
	App.init();
});


