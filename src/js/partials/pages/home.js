App.Home = {

	init: function () {

		this.bindEvents();

	},

	setHeight: function() {
		var height = $(window).height();
		var topHeight = height - $('.header').height() - $('.services').innerHeight();
		$('.top-home').css('height', topHeight);

	},

	bindEvents: function () {

		$(window).resize($.proxy(function () {
			this.setHeight();
		}, this));

		$(window).trigger('resize');

	},

}